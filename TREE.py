import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, f1_score
from sklearn import tree
from sklearn.metrics import roc_auc_score


def decision_tree(X_train, X_test, y_train, y_test):
    """
    决策树
    :return:
    """
    clf = tree.DecisionTreeClassifier()
    clf.fit(X_train, y_train)

    predictions = clf.predict(X_test)
    print(clf.score(X_test, y_test))
    print(classification_report(y_test, predictions))
    print(clf.feature_importances_)
    fea_list = [i for i in zip(X_train.columns,clf.feature_importances_)]
    print(fea_list)
    # print("AUC指标：", roc_auc_score(y_test, predictions))


if __name__ == '__main__':
    path = r"D:\xiangmu\motai\测试\traindata2.csv"
    data = pd.read_csv(path, index_col=0)
    # X = data.iloc[:, :-3]
    X = data.drop(["EIGENVALUE", "label", "MODE"], axis=1)  # 去掉标签列
    y = data["label"]
    X_train, X_test, y_train, y_test = train_test_split(X, y)
    # 4. 特征工程(标准化)
    transfer = StandardScaler()
    x_train = transfer.fit_transform(X_train)
    x_test = transfer.transform(X_test)
    decision_tree(X_train, X_test, y_train, y_test)
