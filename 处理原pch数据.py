import pandas as pd
import re
import os


def get_features(data):
    """
    获取特征列表
    :param data: 特征所在的行组成的列表
    :return: 特征列表、列名
    """
    data1 = data[::2]  # ['210_0        G     111.50564E+00',...]
    data2 = data[1::2]  # ['-CONT-            111.50564E+00',...]
    comp = re.compile(r"G(.*)")
    comp2 = re.compile(r"-CONT-(.*)")
    data_list = []
    col_list = []
    for line1, line2 in zip(data1, data2):
        str1 = re.findall(comp, line1)[0].strip()  # 过滤210_0        G以及两边空格
        str2 = re.findall(comp2, line2)[0].strip()  # 过滤-CONT-        G以及两边空格
        data_list1 = re.findall(r"[^\s]+", str1)[0:3]  # 过滤掉最后一个索引数字
        data_list2 = re.findall(r"[^\s]+", str2)[0:3]
        col_name = re.findall(r"(.*?)G", line1)[0].strip()  # 210_0
        data_list.extend(data_list1)
        data_list.extend(data_list2)
        col_list.append(col_name)
    # col_list = set(col_list)
    return data_list, col_list


def findfiles(path):
    """
    遍历path
    :param path: 需要遍历的文件夹路径
    :return: 向list_dir中添加pch文件路径
    """
    # 首先遍历当前目录所有文件及文件夹
    file_list = os.listdir(path)
    # 循环判断每个元素是否是文件夹还是文件，是文件夹的话，递归
    for file in file_list:  # 利用os.path.join()方法取得路径全名，并存入cur_path变量，否则每次只能遍历一层目录
        cur_path = os.path.join(path, file)
        # 判断是否是文件夹
        if os.path.isdir(cur_path):
            findfiles(cur_path)
        else:  # 判断是否是特定文件名称
            if ".csv" in file:
                list_dir.append(os.path.join(path, file))


def processing_data(data_all):
    """
    数据处理
    :param data_all: 一个pch文件按行读取出来的原始数据
    :return: 处理成特征的df
    """
    sev_list = []  # $EIGENVALUE 索引列表
    title_list = []  # $TITLE 索引列表
    for index, line in enumerate(data_all):
        sev = re.findall(r"\$EIGENVALUE", line)[0] if re.findall(r"\$EIGENVALUE", line) else None  # 获取到$EIGENVALUE行
        title = re.findall(r"\$TITLE", line)[0] if re.findall(r"\$TITLE", line) else None
        if sev:
            sev_list.append(index)  # 将$EIGENVALUE行所在的索引放入列表
        if title:
            title_list.append(index)

    data_list = []  # 数据
    col_list = []  # 列名：编号
    for i in range(len(sev_list)):  # 遍历$EIGENVALUE 索引列表
        if i + 1 == len(sev_list):
            data = data_all[sev_list[i]:]  # 最后一个$EIGENVALUE对应的数据位于末尾
        else:
            data = data_all[sev_list[i]:title_list[i + 1]]  # 数据位于第i个$EIGENVALUE与第i+1个$TITLE之间
        # print(data)
        data0 = data[0]  # $EIGENVALUE  与  MODE所在行
        EIGENVALUE = re.findall("\$EIGENVALUE =(.*?)MODE =.*", data0)[0].strip()  # 取出EIGENVALUE值
        label = re.findall("\$EIGENVALUE =.*?MODE =.*?(\d{1,2}).*", data0)[0]  # 取出MODE值
        one_data_list, col_list = get_features(data[1:])  # 获取特征列，data[1:]：特征所在行
        one_data_list.extend([EIGENVALUE, label])  # 添加EIGENVALUE和label
        data_list.append(one_data_list)
    # len_feature = len(data_list[0])
    column = [f"feature_{i}_{j}" for i in col_list for j in range(6)]
    column.extend(["EIGENVALUE", "label"])
    df1 = pd.DataFrame(data_list, columns=column)
    return df1


def get_sum(inde):
    """
    只获取位移的平方和
    :param inde: 一行数据
    :return: 新的特征
    """
    dic = {}
    for index, i in enumerate(inde):
        if index % 6 == 0:  # 6个特征为一组，取一组的前三个特征
            key = inde.index[index] + "+" + inde.index[index + 1] + "+" + inde.index[index + 2]  # 列名
            sq_sum = inde[index] ** 2 + inde[index + 1] ** 2 + inde[index + 2] ** 2  # 值
            dic[key] = sq_sum
    ser = pd.Series(dic)
    return ser


def get_sum2(inde):
    """
    获取位移和角位移的平方和
    :param inde: 一行数据
    :return: 新的特征
    """
    dic = {}
    for index, i in enumerate(inde):
        if index % 6 == 0:  # 6个特征为一组，取一组的前三个特征
            key = inde.index[index] + "+" + inde.index[index + 1] + "+" + inde.index[index + 2]  # 列名
            key2 = inde.index[index + 3] + "+" + inde.index[index + 4] + "+" + inde.index[index + 5]  # 列名
            sq_sum = inde[index] ** 2 + inde[index + 1] ** 2 + inde[index + 2] ** 2  # 值
            sq_sum2 = inde[index + 3] ** 2 + inde[index + 4] ** 2 + inde[index + 5] ** 2  # 值
            dic[key] = sq_sum
            dic[key2] = sq_sum2
    ser = pd.Series(dic)
    return ser


def get_displacement(data):
    """
    特征工程：获取位移作为特征
    :param data: 数据
    :return: 新的dataframe
    """
    data.dropna(axis=0, inplace=True)  # 删除掉有空值的行
    labels = data[["EIGENVALUE", "label", "MODE"]]  # 标签列
    data_feature = data.drop(["EIGENVALUE", "label", "MODE"], axis=1)  # 去掉标签列
    data_feature = data_feature.astype("float64")
    new_feature = data_feature.apply(get_sum2, axis=1)  # 获取新特征
    new_data = pd.concat([new_feature, labels], axis=1)  # 拼接新特征列与标签列
    return new_data


if __name__ == '__main__':

    list_dir = []  # 用于存储pch文件路径
    path = r"D:\xiangmu\motai\测试"  # 数据存在的目录
    findfiles(path)  # 向list_dir内添加pch文件路径
    print(list_dir)
    df_empty = pd.DataFrame()  # 用以保存所有的数据
    for dir in list_dir:  # 每次处理一个文件
        with open(dir, "r", encoding="utf-8") as f:
            data_all = f.readlines()  # 将数据按行读取到列表里面
        df1 = processing_data(data_all)  # 数据处理为特征与标签
        df_empty = df_empty.append(df1, ignore_index=True)  # 合并到总的dataframe中
    df_empty["MODE"] = df_empty["label"]  # 新建MODE列
    df_empty.loc[df_empty['label'] != "1", "label"] = 0  # 处理label列为0、1
    # print(df_empty)
    input_path = os.path.join(path, "traindata.csv")  # 原始特征文件存储路径
    output_path = os.path.join(path, "traindata2.csv")  # 特征工程文件存储路径
    df_empty.to_csv(input_path)  # 原始特征存出

    # 特征工程
    new_data = get_displacement(df_empty)
    new_data.to_csv(output_path)
    # print(new_data)
